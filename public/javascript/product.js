const categoryInputs = ["Weight", "Size", ["Length", "Width", "Height"]];


const selectEl = document.querySelector("#productType");
const addedInputsContainer = document.querySelector(
    ".inputs_container > .added_inputs"
);




selectEl.addEventListener(
    "change",
    ({ target }) => {
        const id = target.value;
        addedInputsContainer.innerHTML = "";
        if (typeof categoryInputs[id - 1] === "object") {
            categoryInputs[id - 1].map((lableString, i) => {
                createInputElement(lableString);
            });
            return;
        }
        createInputElement(categoryInputs[id - 1]);
    },
    false
);


const createInputElement = (lableString) => {
    addedInputsContainer.insertAdjacentHTML("beforeend",
        `<div class="form-group">
            <label>${lableString}</label>
            <input 
                required 
                type="number" 
                name="${lableString.toLowerCase()}"
                id="${lableString.toLowerCase()}"
                class="form-control form-control-lg" 
                placeholder="Add ${lableString}...">
        </div>`
    );
};