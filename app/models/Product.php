<?php
  class Product {
    private $db;
    
    public function __construct(){
      $this->db = new Database;
    }

    // Get All Products
    public function getProducts(){
      $this->db->query("SELECT * FROM products");

      $results = $this->db->resultset();

      return $results;
    }

    // Add Product
    public function addProduct($data){
      // Prepare Query
      $this->db->query('INSERT INTO products (product_name, product_price, product_sku, category_id) 
      VALUES (:product_name, :product_price, :product_sku, :category_id)');

      // Bind Values
      $this->db->bind(':product_name', $data['product_name']);
      $this->db->bind(':product_price', $data['product_price']);
      $this->db->bind(':product_sku', $data['product_sku']);
      $this->db->bind(':category_id', $data['category_id']);
      
      //Execute
      if($this->db->execute()){
        return true;
      } else {
        return false;
      }
    }

    // Delete Product
    public function delete($id){
      $this->db->query("DELETE FROM `products` WHERE product_id = $id");
      
      // Execute
      if($this->db->execute()){
        return true;
      } else {
        return false;
      }
    }
  }