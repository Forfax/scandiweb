<?php

class Cd {
    private $db;
    
    public function __construct(){
      $this->db = new Database;
    }

    // Get All Products
    public function get(){
      $this->db->query("SELECT * FROM cd");

      $results = $this->db->resultset();

      return $results;
    }

    // Add Product
    public function add($data){
        $productId = $this->db->lastInsertId();
      // Prepare Query
      $this->db->query('INSERT INTO cd (cd_id, size) 
      VALUES (:cd_id, :size)');

      // Bind Values
      $this->db->bind(':cd_id', $productId);
      $this->db->bind(':size', $data['size']);
      
      //Execute
      if($this->db->execute()){
        return true;
      } else {
        return false;
      }
    }

    // Delete Product
    public function delete($id){
      $this->db->query("DELETE FROM `cd` WHERE cd_id = $id");
      
      // Execute
      if($this->db->execute()){
        return true;
      } else {
        return false;
      }
    }
  }