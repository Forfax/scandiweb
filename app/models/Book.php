<?php

class Book {
    private $db;
    
    public function __construct(){
      $this->db = new Database;
    }

    // Get All Products
    public function get(){
      $this->db->query("SELECT * FROM book");

      $results = $this->db->resultset();

      return $results;
    }

    // Add Product
    public function add($data){
        $productId = $this->db->lastInsertId();
      // Prepare Query
      $this->db->query('INSERT INTO book (book_id, weight) 
      VALUES (:book_id, :weight)');

      // Bind Values
      $this->db->bind(':book_id', $productId);
      $this->db->bind(':weight', $data['weight']);
      
      //Execute
      if($this->db->execute()){
        return true;
      } else {
        return false;
      }
    }

    // Delete Product
    public function delete($id){
      $this->db->query("DELETE FROM `book` WHERE book_id = $id");
      
      // Execute
      if($this->db->execute()){
        return true;
      } else {
        return false;
      }
    }
}