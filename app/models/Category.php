<?php

class Category {
    private $db;
    
    public function __construct(){
      $this->db = new Database;
    }

    // Get All Products
    public function getCategories(){
      $this->db->query("SELECT * FROM categories");

      $results = $this->db->resultset();

      return $results;
    }
}