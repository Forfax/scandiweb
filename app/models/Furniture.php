<?php

class Furniture {
    private $db;
    
    public function __construct(){
      $this->db = new Database;
    }

    // Get All Products
    public function get(){
      $this->db->query("SELECT * FROM furniture");

      $results = $this->db->resultset();

      return $results;
    }

    // Add Product
    public function add($data){
        $productId = $this->db->lastInsertId();
      // Prepare Query
      $this->db->query('INSERT INTO furniture (furniture_id, length, width, height) 
      VALUES (:furniture_id, :length, :width, :height)');

      // Bind Values
      $this->db->bind(':furniture_id', $productId);
      $this->db->bind(':length', $data['length']);
      $this->db->bind(':height', $data['height']);
      $this->db->bind(':width', $data['width']);
      
      //Execute
      if($this->db->execute()){
        return true;
      } else {
        return false;
      }
    }

    // Delete Product
    public function delete($id){
      $this->db->query("DELETE FROM `furniture` WHERE furniture_id = $id");
      
      // Execute
      if($this->db->execute()){
        return true;
      } else {
        return false;
      }
    } 
}