<?php require APPROOT . '/views/inc/header.php'; ?>
  <?php flash('product_message'); ?>
  <div class="row mb-3">
    <div class="col-md-6">
      <h1>Products</h1>
    </div>
    <div class="col-md-6">
      <a class="btn btn-primary pull-right" href="<?php echo URLROOT; ?>/products/add"><i class="fa fa-pencil" aria-hidden="true"></i> Add Product</a>  
    </div>
  </div>

  <form action="<?php echo URLROOT; ?>/products/delete" method="post">
    <input type="submit" class="btn btn-danger" name="btnDelete" value="Delete"/>
    <?php foreach($data['products'] as $product) : ?>
    <div class="card-group">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title"><?php echo $product->product_name; ?></h4>
          <p class="card-text"><?php echo $product->product_sku; ?></p>
          <p class="card-text"><?php echo $product->product_price; ?></p>
          
          <input type="checkbox" value="<?php echo $product->product_id; ?>" name="records[]" />
        </div>
      </div>
    </div>
    <?php endforeach; ?>
  </form>

<?php require APPROOT . '/views/inc/footer.php'; ?>