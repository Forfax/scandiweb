<?php require APPROOT . '/views/inc/header.php'; ?>

<a 
    href="<?php echo URLROOT; ?>" 
    class="btn btn-light">
    <i class="fa fa-backward" aria-hidden="true"></i> 
    Back
</a>


<div class="card card-body bg-light mt-5">
    <h2>Add Post</h2>
    <form class="add_product_form" id="product_form" action="<?php echo URLROOT; ?>/products/add" method="post">
        <div class="inputs_container">
            <div class="form-group">
                <label>Name:</label>
                <input 
                    type="text" 
                    required 
                    name="product_name"
                    id="name"
                    class="form-control form-control-lg 
                        <?php echo (!empty($data['Name_err'])) ? 'is-invalid' : ''; ?>" 
                    value="<?php echo $data['product_name']; ?>"
                    placeholder="Add a name...">
                <span class="invalid-feedback"><?php echo $data['Name_err']; ?></span>
            </div>
            <div class="form-group">
                <label>Price:</label>
                <input type="text" 
                required 
                name="product_price"
                id="price"
                class="form-control form-control-lg 
                    <?php echo (!empty($data['Price_err'])) ? 'is-invalid' : ''; ?>" 
                value="<?php echo $data['product_price']; ?>" 
                placeholder="Add a price...">
                <span class="invalid-feedback"><?php echo $data['Price_err']; ?></span>
            </div>
            <div class="form-group">
                <label>SKU:</label>
                <input type="text" 
                required 
                name="product_sku" 
                id="sku" 
                class="form-control form-control-lg 
                    <?php echo (!empty($data['SKU_err'])) ? 'is-invalid' : ''; ?>" 
                value="<?php echo $data['product_sku']; ?>" placeholder="Add a SKU...">
                <span class="invalid-feedback"><?php echo $data['SKU_err']; ?></span>
            </div>

            <h4 class="mt-5">Choose product type</h4>
            <select name="category_id" id="productType" class="form-select">
                <option disabled selected>
                    Select category...
                </option>
                <?php
                foreach ($data['categories'] as $category) {
                ?>
                    <option value="<?php echo $category->category_id; ?>">
                        <?php echo $category->category_name; ?>
                    </option>
                <?php
                }
                ?>
            </select>
            <div class="added_inputs mb-5"></div>
        </div>
        <input type="submit" class="btn btn-success" value="Submit">
    </form>
</div>

<script type="text/javascript" src="../javascript/product.js" />
<?php require APPROOT . '/views/inc/footer.php'; ?>