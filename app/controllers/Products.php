<?php
  class Products extends Controller{
    public function __construct(){
      // Load Models
      $this->productModel = $this->model('Product');
      $this->categoryModel = $this->model('Category');
      $this->cdModel = $this->model('Cd');
      $this->bookModel = $this->model('Book');
      $this->furnitureModel = $this->model('Furniture');
    }

    // Load All Products
    public function index(){
      $products = $this->productModel->getProducts();
      
      
      $data = [
        'products' => $products
      ];
      
      $this->view('products/index', $data);
    }

    // Add Product
    public function add(){
      $categories = $this->categoryModel->getCategories();
      if($_SERVER['REQUEST_METHOD'] == 'POST'){
        // Sanitize POST
        $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        
        $data = [
          'product_name' => trim($_POST['product_name']),
          'product_sku' => trim($_POST['product_sku']), 
          'product_price' => trim($_POST['product_price']),
          'category_id' => trim($_POST['category_id']),
          'size' => trim($_POST['size']??''),
          'weight' => trim($_POST['weight']??''),
          'length' => trim($_POST['length']??''),
          'height' => trim($_POST['height']??''),
          'width' => trim($_POST['width']??''),
          'Name_err' => '',
          'Price_err' => '',
          'Producttype_err' => '',
          'typeval_err' => '',
          'SKU_err' => ''
        ];

        if(empty($data['product_name'])){
          $data['Name_err'] = 'Please enter name';
          // Validate name
          if(empty($data['product_price'])){
            $data['Price_err'] = 'Please enter the Price';
          }

          if(!is_float(floatval($data['product_price']))){
            $data['Price_err'] = 'Please enter the valid price';
          }

          if(empty($data['category_id'])){
            $data['Producttype_err'] = 'Please enter Producttype';
          }

          if(empty($data['product_sku'])){
            $data['SKU_err'] = 'Please enter SKU';
          }
        }

        // Make sure there are no errors
        if(empty($data['Name_err']) && empty($data['Price_err']) && empty($data['Producttype_err']) && empty($data['typeval_err']) && empty($data['SKU_err'])) {
          // Validation passed
          //Execute
          if($this->productModel->addProduct($data)){
            $model = ["book", "cd", "furniture"][intval($data['category_id'])-1]."Model";
            $this->$model->add($data);
            // Redirect to login
            flash('product_message', 'Product Added');
            redirect('products');
          } else {
            die('Something went wrong');
          }
        } else {
          // Load view with errors
          $this->view('products/add', $data);
        }

      } else {
        $data = [
          'product_name' => '',
          'product_sku' => '',
          'product_price' => '',
          'category_id' => ''
        ];

        $data['categories'] = $categories;

        $this->view('products/add', $data);

      }
    }

    // Delete Post
    public function delete(){
      if($_SERVER['REQUEST_METHOD'] == 'POST'){
        // Sanitize POST
        $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        foreach($_POST['records'] as $recordId) {
          if($this->productModel->delete($recordId))
          {
            flash('product_message', 'product Removed');
            redirect('products');
          }
          else 
          {
            die('Something went wrong');
          }
        }
        
      }
    }
  }