<?php

class Categories extends Controller {
    public function __construct(){
        // Load Models
        $this->categoryModel = $this->model('Category');
    }

      public function index(){
        $categories = $this->categoryModel->getCategories();
  
        $data = [
          'categories' => $categories
        ];
        
        $this->view('products/add', $data);
      }
}